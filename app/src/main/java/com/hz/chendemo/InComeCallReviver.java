package com.hz.chendemo;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.telephony.TelephonyManager;

import org.greenrobot.eventbus.EventBus;

import static com.hz.chendemo.PhoneFloatActivity.END_CALL;

public class InComeCallReviver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

//        基本的逻辑就是 来了电话 监听电话号码；
        //如果这个电话号码在你的数据信息中，那么你需要把你这个tip 提示给用户；
        //Demo 还需要处理的事情：目前完成了来电监听 显示tip的功能；
        //接下来需要做：构建service ，在一个永不杀死的进程中 监听；
        
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Service.TELEPHONY_SERVICE);
        int callState = tManager.getCallState();

        switch (callState) {
            case TelephonyManager.CALL_STATE_RINGING:

                String phoneNumber = intent.getStringExtra("incoming_number");
                if(phoneNumber == null){
                    return;
                }

                Intent myIntent =new Intent(context,PhoneFloatActivity.class);
                myIntent.putExtra("incoming_number", phoneNumber);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(myIntent);

                break;

            case TelephonyManager.CALL_STATE_IDLE:
                EventBus.getDefault().post(END_CALL);
                break;
        }
    }
}
