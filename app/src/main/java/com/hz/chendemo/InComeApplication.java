package com.hz.chendemo;

import android.app.Application;
import android.content.Context;

import com.hjq.toast.ToastUtils;

public class InComeApplication extends Application {

    private static Context mContext;
    public static Context getInstance() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();

        ToastUtils.init(this,new YToastStyle(this));

    }

}
