package com.hz.chendemo;

import android.content.Context;
import android.view.Gravity;

import com.hjq.toast.style.BaseToastStyle;

public class YToastStyle extends BaseToastStyle {

    public YToastStyle(Context context) {
        super(context);
    }

    @Override
    public int getCornerRadius() {
        return dp2px(20);
    }

    @Override
    public int getBackgroundColor() {
        return 0XFFA2A1A4;
    }

    @Override
    public int getTextColor() {
        return 0XEEFFFFFF;
    }

    @Override
    public int getZ() {
        return 0;
    }

    @Override
    public float getTextSize() {
        return sp2px(14);
    }

    @Override
    public int getPaddingStart() {
        return dp2px(20);
    }

    @Override
    public int getPaddingTop() {
        return dp2px(10);
    }

    @Override
    public int getGravity() {
        return Gravity.BOTTOM;
    }


    @Override
    public int getYOffset() {
        return dp2px(30);
    }
}
